import { Routes } from '@angular/router';
import { YourFeedComponent } from './component/your-feed/your-feed.component';

export const yourFeedRoutes: Routes = [
  { path: '', component: YourFeedComponent },
];
