import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RegisterComponent } from './auth/components/register/register.component';
import { TopbarComponent } from './shared/components/topbar/topbar.component';
import { Store } from '@ngrx/store';
import { authActions } from './auth/store/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  standalone: true,
  imports: [RouterOutlet, TopbarComponent],
})
export class AppComponent implements OnInit {
  title = 'mediumClone';
  constructor(private store: Store) {}
  ngOnInit(): void {
    this.store.dispatch(authActions.getcurrentuser());
  }
}
