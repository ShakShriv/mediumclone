import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './auth/components/register/register.component';
import { BackendErrorMessagesComponent } from './shared/components/backend-error-messages/backend-error-messages.component';
import { LoginComponent } from './auth/components/login/login.component';
import { TopbarComponent } from './shared/components/topbar/topbar.component';
import { GlobalFeedComponent } from './global-feed/global-feed.component';
import FeedComponent from './shared/components/feed/feed.component';
import { BannerComponent } from './shared/components/banner/banner.component';
import { ErrorMessageComponent } from './shared/components/error-message/error-message.component';
import { PaginationComponent } from './shared/components/pagination/pagination.component';
import { TagListComponent } from './shared/components/tag-list/tag-list.component';
import { PopularTagsComponent } from './shared/components/popular-tags/popular-tags.component';
import { FeedTogglerComponent } from './shared/components/feed-toggler/feed-toggler.component';
import { YourFeedComponent } from './YourFeed/component/your-feed/your-feed.component';
import { ArticleComponent } from './articles/components/article/article.component';
import { CreateArticleComponent } from './createArticle/component/create-article/create-article.component';
import { ArticleFormComponent } from './shared/components/article-form/article-form.component';
import { SettingsComponent } from './userSettings/settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    BackendErrorMessagesComponent,
    LoginComponent,
    TopbarComponent,
    GlobalFeedComponent,
    FeedComponent,
    BannerComponent,
    ErrorMessageComponent,
    PaginationComponent,
    TagListComponent,
    PopularTagsComponent,
    FeedTogglerComponent,
    YourFeedComponent,
    ArticleComponent,
    CreateArticleComponent,
    ArticleFormComponent,
    SettingsComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
