import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';
import { ArticleFormComponent } from 'src/app/shared/components/article-form/article-form.component';
import { ArticleFormValueInterface } from 'src/app/shared/types/articleFormValue';
import { articlesActions } from '../../../articles/store/article.actions';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css'],
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule,
    CommonModule,
    ArticleFormComponent,
  ],
})
export class CreateArticleComponent {
  constructor(private store: Store) {}
  initialValues: ArticleFormValueInterface = {
    body: '',
    tagList: [],
    description: '',
    title: '',
  };
  onSubmit(article: ArticleFormValueInterface) {
    console.warn('FORM VALUE', article);
    this.store.dispatch(articlesActions.createArticle({ article }));
  }
}
