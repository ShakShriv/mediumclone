import { Routes } from '@angular/router';
import { CreateArticleComponent } from './components/create-article/create-article.component';

export const routes: Routes = [{ path: '', component: CreateArticleComponent }];
