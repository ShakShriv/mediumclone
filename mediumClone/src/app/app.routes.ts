import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  {
    path: 'register',
    loadChildren: () =>
      import('src/app/auth/auth.routes').then((m) => m.registerRoutes),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('src/app/auth/auth.routes').then((m) => m.loginRoutes),
  },
  {
    path: '',
    loadChildren: () =>
      import('src/app/globalFeed/global.routes').then((m) => m.globalRoutes),
  },
  {
    path: 'feed',
    loadChildren: () =>
      import('src/app/YourFeed/yourFeed.routes').then((m) => m.yourFeedRoutes),
  },
  {
    path: 'tag/:slug',
    loadChildren: () =>
      import('src/app/tagFeed/tagFeed.routes').then((m) => m.routes),
  },
  {
    path: 'article/new',
    loadChildren: () =>
      import('src/app/createArticle/newArticle.routes').then((m) => m.routes),
  },
  {
    path: 'article/:slug',
    loadChildren: () =>
      import('src/app/articles/article.routes').then((m) => m.routes),
  },
  {
    path: 'settings',
    loadChildren: () =>
      import('src/app/userSettings/setting.routes').then((m) => m.routes),
  },
];
