import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { PopularTagType } from '../types/popularTag.type';
import { PopularTagsResponseI } from '../types/popularTagsResponse.interface';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root',
})
export class PopularTagService {
  tagsUrl: string = '/tags';
  constructor(private http: HttpClient) {}

  getPopularTags(): Observable<PopularTagType[]> {
    return this.http
      .get<PopularTagsResponseI>(environment.API_URL + this.tagsUrl)
      .pipe(map((res: PopularTagsResponseI) => res.tags));
  }
}
