import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { getFeedResponseI } from '../types/getFeedResponse.interface';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root',
})
export class FeedService {
  constructor(private http: HttpClient) {}

  getFeed(url: string): Observable<getFeedResponseI> {
    const apiUrl = environment.API_URL + url;
    return this.http.get<getFeedResponseI>(apiUrl);
  }
}
