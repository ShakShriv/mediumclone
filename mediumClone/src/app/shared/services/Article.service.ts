import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, tap } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { getArticleResponseI } from '../types/getArticleResponse.interface';
import { ArticleI } from '../types/article.interface';
import { ArticleFormValueInterface } from '../types/articleFormValue';
import { CreateArticleReqI } from '../types/createArticleRequest.interface';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  constructor(private http: HttpClient) {}

  getArticle(slug: string): Observable<ArticleI> {
    const apiUrl = `${environment.API_URL}/articles/${slug}`;
    return this.http
      .get<getArticleResponseI>(apiUrl)
      .pipe(map((res) => res.article));
  }
  deleteArticle(slug: string): Observable<any> {
    const apiUrl = `${environment.API_URL}/articles/${slug}`;
    return this.http.delete<getArticleResponseI>(apiUrl);
  }

  createArticle(article: ArticleFormValueInterface): Observable<ArticleI> {
    const req: CreateArticleReqI = { article };
    const url = `${environment.API_URL}/articles`;
    return this.http.post<getArticleResponseI>(url, req).pipe(
      map((res) => res.article),
      tap(console.info)
    );
  }
}
