import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UtiliesService {
  constructor() {}
  range(start: number, end: number) {
    return [...Array(end - start).keys()].map((el) => el + 1);
  }
}
