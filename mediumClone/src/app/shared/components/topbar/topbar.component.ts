import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, combineLatest, forkJoin } from 'rxjs';
import { CurrentUserI } from '../../types/currentUserInterface';
import { selectCurrentUser } from 'src/app/auth/store/reducers';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css'],
  standalone: true,
  imports: [CommonModule, RouterLink],
})
export class TopbarComponent implements OnInit {
  data$!: Observable<any>;
  constructor(private store: Store) {}

  ngOnInit(): void {
    this.data$ = combineLatest({
      currntUser: this.store.select(selectCurrentUser),
    });
  }
}
