import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { Observable } from 'rxjs';
import { CurrentUserI } from '../../types/currentUserInterface';
import { Store } from '@ngrx/store';
import { selectCurrentUser } from 'src/app/auth/store/reducers';

@Component({
  selector: 'app-feed-toggler',
  templateUrl: './feed-toggler.component.html',
  styleUrls: ['./feed-toggler.component.css'],
  standalone: true,
  imports: [CommonModule, RouterLink, RouterLinkActive],
})
export class FeedTogglerComponent implements OnInit {
  currentUser$!: Observable<CurrentUserI | null | undefined>;

  constructor(private store: Store) {}
  ngOnInit(): void {
    this.currentUser$ = this.store.select(selectCurrentUser);
  }
  @Input() tagName?: string; // if we are not on popularTagPage we don't need to provide it
}
