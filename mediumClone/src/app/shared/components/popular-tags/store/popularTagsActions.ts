import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { PopularTagType } from 'src/app/shared/types/popularTag.type';

export const popularTagsAction = createActionGroup({
  source: 'Popular Tags',
  events: {
    'Get Popular Tags': emptyProps(),
    'Get Popular Tags Success': props<{ popularTags: PopularTagType[] }>(),
    'Get Popular Tags Failed': emptyProps(),
  },
});
