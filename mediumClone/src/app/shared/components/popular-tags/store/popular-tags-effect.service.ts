import { Injectable } from '@angular/core';
import { PopularTagService } from '../../../services/popular-tag.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { popularTagsAction } from './popularTagsActions';
import { concatMap, map, catchError, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PopularTagsEffect {
  constructor(
    private PopularTagService: PopularTagService,
    private actions: Actions
  ) {}

  popularTagsEffect$ = createEffect(() =>
    this.actions.pipe(
      ofType(popularTagsAction.getPopularTags),
      concatMap(() =>
        this.PopularTagService.getPopularTags().pipe(
          map((popularTags) =>
            popularTagsAction.getPopularTagsSuccess({ popularTags })
          )
        )
      ),

      catchError(() => of(popularTagsAction.getPopularTagsFailed()))
    )
  );
}
