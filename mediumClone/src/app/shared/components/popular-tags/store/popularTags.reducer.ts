import { createFeature, createReducer, on } from '@ngrx/store';
import { BackendErrorsI } from 'src/app/shared/types/backendErrors.interface';
import { PopularTagType } from 'src/app/shared/types/popularTag.type';
import { popularTagsAction } from './popularTagsActions';
import { state } from '@angular/animations';

export interface PopularTagsState {
  isLoading: boolean;
  error: BackendErrorsI | null;
  data: PopularTagType[] | null;
}
const initalState: PopularTagsState = {
  data: null,
  error: null,
  isLoading: false,
};

const popularTagsFeature = createFeature({
  name: 'popularTags',
  reducer: createReducer(
    initalState,
    on(popularTagsAction.getPopularTags, (state) => {
      return {
        ...state,
        isLoading: true,
      };
    }),
    on(popularTagsAction.getPopularTagsSuccess, (state, { popularTags }) => {
      return {
        ...state,
        isLoading: false,
        data: popularTags,
      };
    }),
    on(popularTagsAction.getPopularTagsFailed, (state) => {
      return {
        ...state,
        isLoading: false,
        data: null,
      };
    })
  ),
});

export const {
  name: popularTagFeatureKey,
  reducer: popularTagsReducer,
  selectData: selectPopularTags,
  selectError: selectPopularTagsError,
  selectIsLoading: selectPopularTagsIsLoading,
} = popularTagsFeature;
