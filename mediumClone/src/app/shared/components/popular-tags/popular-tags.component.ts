import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';
import { popularTagsAction } from './store/popularTagsActions';
import { Observable, combineLatest } from 'rxjs';
import {
  selectPopularTags,
  selectPopularTagsError,
  selectPopularTagsIsLoading,
} from './store/popularTags.reducer';
import { BackendErrorsI } from '../../types/backendErrors.interface';
import { PopularTagType } from '../../types/popularTag.type';
import { ErrorMessageComponent } from '../error-message/error-message.component';

@Component({
  selector: 'app-popular-tags',
  templateUrl: './popular-tags.component.html',
  styleUrls: ['./popular-tags.component.css'],
  standalone: true,
  imports: [CommonModule, RouterLink, ErrorMessageComponent],
})
export class PopularTagsComponent implements OnInit {
  data$!: Observable<{
    popularTags: PopularTagType[] | null; //
    error: BackendErrorsI | null;
    isLoading: boolean; // Un booléen indiquant si la requête est en cours
  }>;
  constructor(private store: Store) {}
  ngOnInit(): void {
    this.store.dispatch(popularTagsAction.getPopularTags());
    this.data$ = combineLatest({
      popularTags: this.store.select(selectPopularTags),
      error: this.store.select(selectPopularTagsError),
      isLoading: this.store.select(selectPopularTagsIsLoading),
    });
  }
}
