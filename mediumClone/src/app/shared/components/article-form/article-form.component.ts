import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { RouterLink } from '@angular/router';
import { ArticleFormValueInterface } from '../../types/articleFormValue';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css'],
  standalone: true,
  imports: [RouterLink, ReactiveFormsModule, CommonModule],
})
export class ArticleFormComponent implements OnInit {
  constructor(private fb: FormBuilder) {}
  @Input() articleValues?: ArticleFormValueInterface;
  @Input() isSubmitting: boolean = false;

  @Output() articleValuesChange = new EventEmitter<ArticleFormValueInterface>();

  form!: FormGroup;

  ngOnInit(): void {
    this.form = this.fb.nonNullable.group({
      title: '',
      description: '',
      body: '',
      tagList: '',
    });
    this.initForm();
  }

  initForm() {
    if (!!this.articleValues) {
      this.form.patchValue({
        title: this.articleValues?.title,
        tagList: this.articleValues?.tagList.join(' '),
        description: this.articleValues?.description,
        body: this.articleValues?.body,
      });
    }
  }

  onSubmit() {
    let values = this.form.getRawValue();
    const formValues: ArticleFormValueInterface = {
      ...values,
      tagList: values.tagList.split(' '),
    };

    this.articleValuesChange.emit(formValues);
  }
}
