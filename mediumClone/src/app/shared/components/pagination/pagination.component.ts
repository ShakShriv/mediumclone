import { CommonModule } from '@angular/common';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { environment } from 'src/environments/environment';
import { UtiliesService } from '../../services/utilies.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
  standalone: true,
  imports: [CommonModule, RouterLink],
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() total: number = 1;
  @Input() currentPage: number = 0;
  @Input() limit: number = 1;
  @Input() url: string = '';

  pages: number[] = [];

  pageCount: number = 1;
  constructor(private _utilsService: UtiliesService) {}
  ngOnChanges(changes: SimpleChanges): void {
    console.info(changes);
    if (changes['total']?.currentValue && changes['limit']?.currentValue) {
      this.pageCount = Math.ceil(this.total / this.limit);
      this.pages =
        this.pageCount > 0 ? this._utilsService.range(1, this.pageCount) : [];
      console.warn('pages', this.pages);
    }
  }
  ngOnInit(): void {}
}
 