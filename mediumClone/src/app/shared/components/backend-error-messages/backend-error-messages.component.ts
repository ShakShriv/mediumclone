import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { BackendErrorsI } from '../../types/backendErrors.interface';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-backend-error-messages',
  templateUrl: './backend-error-messages.component.html',
  styleUrls: ['./backend-error-messages.component.css'],
  standalone: true,
  imports: [CommonModule],
})
export class BackendErrorMessagesComponent implements OnInit, OnChanges {
  @Input() backendErrors: BackendErrorsI = {};

  errorMessages: string[] = [];

  constructor() {}
  ngOnInit(): void {}
  ngOnChanges(): void {
    this.errorMessages = Object.keys(this.backendErrors).map((propName) => {
      const message: string = this.backendErrors[propName].join(' ');
      return `${propName} ${message}`;
    });
  }
}
