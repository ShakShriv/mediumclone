import {
  Component,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { feedActions } from './store/feedActions';
import { Observable, combineLatest, tap, of } from 'rxjs';
import {
  selectFeedData,
  selectFeedError,
  selectIsLoading,
} from './store/feedReducer'; 
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { ErrorMessageComponent } from '../error-message/error-message.component';
import { PaginationComponent } from '../pagination/pagination.component';
import { environment } from 'src/environments/environment.development';
import queryString from 'query-string';
import { TagListComponent } from '../tag-list/tag-list.component';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    ErrorMessageComponent,
    PaginationComponent,
    TagListComponent,
  ],
})
export class FeedComponent implements OnInit, OnChanges {
  @Input() apiUrl: string = '';
  data$!: Observable<any>;
  limit = environment.limitPagination;
  baseUrl: string = '';
  currentPage: number = 1;

  constructor(
    private store: Store,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}
  ngOnChanges(changes: SimpleChanges): void {
    console.log('feed compo changes', changes);
   if (
     changes['apiUrl'] &&
     changes['apiUrl'].currentValue !== changes['apiUrl'].previousValue
   )
     this.fetchFeed();
  }

  ngOnInit(): void {
    this.data$ = combineLatest({
      isLoading: this.store.select(selectIsLoading),
      feedData: this.store.select(selectFeedData),
      feedError: this.store.select(selectFeedError),
    });

    this.baseUrl = this._router.url.split('?')[0]; // post?page=1 => post
    this._route.queryParams.pipe(tap(console.info)).subscribe((params) => {
      this.currentPage = +params['page'] || 1;
      this.fetchFeed();
    });
  }

  fetchFeed() {
    const offset = this.currentPage * this.limit - this.limit;
    const parseUrl = queryString.parseUrl(this.apiUrl);
    const stringifiedParam = queryString.stringify({
      limit: this.limit,
      offset: offset,
      ...parseUrl.query, // api url dans le cadre de la recup de feed via un tag : articles?tag=... tag-feed ligne 36
    });
    // artciles?tag=welcome pour récuperer les feed dont le popular tag est welcome
    console.log('parseURL ', parseUrl);
    const apiUrlWithParam = `${parseUrl.url}?${stringifiedParam}`;
    this.store.dispatch(feedActions.getFeed({ url: apiUrlWithParam }));
    /*
     /articles?limit=20&offset=0
     page 1 -- offset =0 
     page 2 -- offset =20 
     page 3 -- offset =40 
     */
  }
}
