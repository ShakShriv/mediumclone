import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FeedService } from '../../../services/feed.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { feedActions } from './feedActions';
import { catchError, concatMap, map, of } from 'rxjs';
import { getFeedResponseI } from 'src/app/shared/types/getFeedResponse.interface';

@Injectable({ providedIn: 'root' })
export class FeedEffect {
  constructor(private feedService: FeedService, private actions: Actions) {}

  feedEffect$ = createEffect(() =>
    this.actions.pipe(
      ofType(feedActions.getFeed),
      concatMap(({ url }) =>
        this.feedService.getFeed(url).pipe(
          map((resp: getFeedResponseI) =>
            feedActions.getFeedSucess({ feed: resp })
          ),
          catchError((err) => of(feedActions.getFeedFailure()))
        )
      )
    )
  );
}
