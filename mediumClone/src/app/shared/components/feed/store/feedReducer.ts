import { createFeature, createReducer, on } from '@ngrx/store';
import { feedState } from './feedState.interface';
import { feedActions } from './feedActions';
import {
  routerNavigatedAction,
  routerNavigationAction,
} from '@ngrx/router-store';

const initialState: feedState = {
  data: null,
  error: null,
  isLoading: false,
};

const feedFeature = createFeature({
  name: 'feed',
  reducer: createReducer(
    initialState,
    on(feedActions.getFeed, (state: feedState, { url }) => {
      return {
        ...state,
        isLoading: true,
      };
    }),
    on(feedActions.getFeedSucess, (state: feedState, { feed }) => {
      return {
        ...state,
        isLoading: false,
        data: feed,
      };
    }),
    on(feedActions.getFeedFailure, (state: feedState) => {
      return {
        ...state,
        isLoading: false,
      };
    }),
    on(routerNavigationAction, () => initialState) // a chaque fois qu'on change de page, pdt que les nv feed sont chargé il faut effacé les  ancien feed donc revenir à l'état initial
  ),
});

export const {
  name: feedFeatureKey,
  reducer: feedReducer,
  selectData: selectFeedData,
  selectError: selectFeedError,
  selectFeedState,
  selectIsLoading,
} = feedFeature;
