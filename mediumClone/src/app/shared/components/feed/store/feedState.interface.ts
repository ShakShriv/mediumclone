import { getFeedResponseI } from 'src/app/shared/types/getFeedResponse.interface';

export interface feedState {
  isLoading: boolean; // for showing spinner,
  error: string | null;
  data: getFeedResponseI | null;
}
