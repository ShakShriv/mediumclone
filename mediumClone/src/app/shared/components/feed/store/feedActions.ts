import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { getFeedResponseI } from 'src/app/shared/types/getFeedResponse.interface';

export const feedActions = createActionGroup({
  source: 'Feed',
  events: {
    'Get Feed': props<{ url: string }>(),
    'Get Feed Sucess': props<{ feed: getFeedResponseI }>(),
    'Get Feed Failure': emptyProps(),
  },
});
