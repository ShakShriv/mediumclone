import { CurrentUserI } from './currentUserInterface';

export type Profile = Pick<
  CurrentUserI,
  'username' | 'bio' | 'image' | 'following'
>;
