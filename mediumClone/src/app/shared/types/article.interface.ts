import { PopularTagType } from './popularTag.type';
import { Profile } from './profile.interface';

export interface ArticleI {
  slug: string;
  title: string;
  description: string;
  body: string;
  tagList: PopularTagType[];
  createdAt: string;
  updatedAt: string;
  favorited: false;
  favoritesCount: 0;
  author: Profile;
}
