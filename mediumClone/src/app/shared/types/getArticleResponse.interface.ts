import { ArticleI } from './article.interface';

export interface getArticleResponseI {
  article: ArticleI;
}
