export interface ArticleFormValueInterface {
  title: string;
  description: string;
  body: string;
  tagList: string[];
}
