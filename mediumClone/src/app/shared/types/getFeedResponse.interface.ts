import { ArticleI } from './article.interface';

export interface getFeedResponseI {
  articles: ArticleI[];
  articlesCount: number;
}
