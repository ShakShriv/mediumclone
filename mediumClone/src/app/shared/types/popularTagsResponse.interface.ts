import { PopularTagType } from './popularTag.type';

export interface PopularTagsResponseI {
  tags: PopularTagType[];
}
