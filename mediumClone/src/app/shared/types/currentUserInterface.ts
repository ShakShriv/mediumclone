export interface CurrentUserI {
  email: string;
  token: string;
  username: string;
  bio: string | null;
  image: string;
  following?: boolean;
}
export type UpdtUser = Omit<CurrentUserI, 'token' | 'following'>;
