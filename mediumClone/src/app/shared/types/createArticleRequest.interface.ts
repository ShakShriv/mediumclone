import { ArticleFormValueInterface } from './articleFormValue';

export interface CreateArticleReqI {
  article: ArticleFormValueInterface;
}
