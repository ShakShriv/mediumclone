import {
  createAction,
  props,
  createActionGroup,
  emptyProps,
} from '@ngrx/store';
import { UserI, RegisterRequestI } from '../types/registerRequest.interface';
import { CurrentUserI } from '../../shared/types/currentUserInterface';
import { BackendErrorsI } from '../../shared/types/backendErrors.interface';
import { LoginRequest } from '../types/loginRequest.type';

export const authActions = createActionGroup({
  source: 'Auth',
  events: {
    Register: props<{ request: RegisterRequestI }>(),
    'Register Sucess': props<{ user: CurrentUserI }>(),
    'Register Failure': props<{ errors: BackendErrorsI }>(),
    Login: props<{ request: LoginRequest }>(),
    'Login Sucess': props<{ user: CurrentUserI }>(),
    'Login Failure': props<{ errors: BackendErrorsI }>(),
    getCurrentUser: emptyProps(),
    'getCurrentUser Sucess': props<{ user: CurrentUserI }>(),
    'getCurrentUser Failure': emptyProps(),
    'Update Current User': props<{ user: Partial<CurrentUserI> }>(),
    'Update Current User Success': props<{ user: CurrentUserI }>(),
    'Update Current User Failure': props<{ errors: BackendErrorsI }>(),
  },
});
