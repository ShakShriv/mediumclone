import { createReducer, createFeature, on } from '@ngrx/store';
import { AuthState } from '../types/authState.interfaces';
import { authActions } from './actions';
import { routerNavigatedAction } from '@ngrx/router-store';
import { state } from '@angular/animations';

const initalState: AuthState = {
  isSubmitting: false,
  currentUser: undefined,
  isLoading: false,
  backendErrors: null,
};

const authFeature = createFeature({
  name: 'auth',
  reducer: createReducer(
    initalState,
    on(authActions.register, (state: AuthState, { request }): AuthState => {
      {
        return {
          ...state,
          isSubmitting: true,
          backendErrors: null,
        };
      }
    }),
    on(authActions.registerSucess, (state: AuthState, { user }): AuthState => {
      {
        return {
          ...state,
          isSubmitting: false,
          currentUser: user,
        };
      }
    }),
    on(
      authActions.registerFailure,
      (state: AuthState, { errors }): AuthState => {
        {
          return {
            ...state,
            isSubmitting: false,
            backendErrors: errors,
          };
        }
      }
    ),
    on(authActions.login, (state: AuthState, { request }): AuthState => {
      {
        return {
          ...state,
          isSubmitting: true,
          backendErrors: null,
        };
      }
    }),
    on(authActions.loginSucess, (state: AuthState, { user }): AuthState => {
      {
        return {
          ...state,
          isSubmitting: false,
          currentUser: user,
        };
      }
    }),
    on(authActions.loginFailure, (state: AuthState, { errors }): AuthState => {
      {
        return {
          ...state,
          isSubmitting: false,
          backendErrors: errors,
        };
      }
    }),
    on(routerNavigatedAction, (state, action) => {
      return {
        ...state,
        backendErrors: null,
      };
    }),
    on(authActions.getcurrentuser, (state: AuthState): AuthState => {
      {
        return {
          ...state,
          isLoading: true,
        };
      }
    }),
    on(
      authActions.getcurrentuserSucess,
      (state: AuthState, { user }): AuthState => {
        {
          return {
            ...state,
            isLoading: false,
            currentUser: user,
          };
        }
      }
    ),
    on(authActions.getcurrentuserFailure, (state: AuthState): AuthState => {
      {
        return {
          ...state,
          isLoading: false,
          currentUser: null,
        };
      }
    }),
    on(authActions.updateCurrentUser, (state: AuthState): AuthState => {
      return {
        ...state,
        isLoading: true,
        backendErrors: null,
      };
    }),
    on(
      authActions.updateCurrentUserFailure,
      (state: AuthState, { errors }): AuthState => {
        return {
          ...state,
          backendErrors: errors,
          isSubmitting: false,
        };
      }
    ),
    on(
      authActions.updateCurrentUserSuccess,
      (state: AuthState, { user }): AuthState => {
        return {
          ...state,
          currentUser: user,
          isSubmitting: false,
          backendErrors: null,
        };
      }
    )
  ),
});

export const {
  name: authFeatureKey,
  reducer: authReducer,
  selectIsSubmitting,
  selectAuthState,
  selectCurrentUser,
  selectBackendErrors,
  selectIsLoading,
} = authFeature;
