import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AuthService } from '../services/auth.service';
import { authActions } from './actions';
import { concatMap, map, catchError, throwError, of, tap } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { BackendErrorsI } from 'src/app/shared/types/backendErrors.interface';
import { PersistanceService } from '../../shared/services/persistance.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffect {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private persistanceService: PersistanceService,
    private router: Router
  ) {}
  redirectionAfterRegisterSucessEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.registerSucess),
        tap(() => this.router.navigate(['/'])),
        tap(() => console.info('redirectionAfterRegisterSucessEffect'))
      ),
    {
      dispatch: false,
    }
  );
  registerEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.register),
      concatMap(({ request }) =>
        this.authService.register(request).pipe(
          map((user) => {
            this.persistanceService.set('accessToken', user.token);
            return authActions.registerSucess({ user });
          })
        )
      ),
      catchError((err: HttpErrorResponse) => {
        console.error(err);
        return of(
          authActions.registerFailure({
            errors: err.error.errors,
          })
        );
      })
    )
  );
  getCurrentUserEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.getcurrentuser),
      concatMap(() =>
        this.authService.getCurrentUser().pipe(
          map((user) => {
            const token: unknown = this.persistanceService.get('accessToken');
            return !token
              ? authActions.getcurrentuserFailure()
              : authActions.getcurrentuserSucess({ user });
          })
        )
      ),
      catchError((err: HttpErrorResponse) => {
        console.error(err);
        return of(authActions.getcurrentuserFailure());
      })
    )
  );

  updateCurrentUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.updateCurrentUser),
      concatMap(({ user }) =>
        this.authService
          .updateCurrentUser(user)
          .pipe(map((user) => authActions.updateCurrentUserSuccess({ user })))
      ),
      catchError((err: HttpErrorResponse) =>
        of(authActions.updateCurrentUserFailure({ errors: err.error?.erros }))
      )
    )
  );
  redirectionAfterLoginSucessEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.loginSucess),
        tap(() => this.router.navigate(['/'])),

        tap(() => console.info('redirectionAfterRegisterSucessEffect'))
      ),
    {
      dispatch: false,
    }
  );
  LoginEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.login),
      concatMap(({ request }) =>
        this.authService.login(request).pipe(
          tap(console.warn),
          map((user) => {
            this.persistanceService.set('accessToken', user.token);
            return authActions.loginSucess({ user });
          }),
          catchError((err: any) => {
            console.error('effeft', err);
            return of(
              authActions.loginFailure({
                errors: err.error.errors,
              })
            );
          })
        )
      )
    )
  );
}

  
