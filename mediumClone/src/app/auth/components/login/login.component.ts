import { Component } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ReactiveFormsModule,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable, combineLatest } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { authActions } from '../../store/actions';
import { selectIsSubmitting, selectBackendErrors } from '../../store/reducers';
import { AuthState } from '../../types/authState.interfaces';
import { RegisterRequestI } from '../../types/registerRequest.interface';
import { LoginRequest } from '../../types/loginRequest.type';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
import { BackendErrorMessagesComponent } from 'src/app/shared/components/backend-error-messages/backend-error-messages.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule,
    CommonModule,
    BackendErrorMessagesComponent,
  ],
})
export class LoginComponent {
  form!: FormGroup;
  /*   isSubmitting$!: Observable<boolean>;
  backendErrors$!: Observable<BackendErrorsI | null>; */
  data$!: Observable<any>;
  constructor(
    private fb: FormBuilder,
    private store: Store<{ auth: AuthState }>
  ) {}
  ngOnInit(): void {
    this.form = this.fb.nonNullable.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

    /*     this.isSubmitting$ = this.store.select(selectIsSubmitting);
    this.backendErrors$ = this.store.select(selectBackendErrors); */
    this.data$ = combineLatest({
      isSubmitting: this.store.select(selectIsSubmitting),
      backendErrors: this.store.select(selectBackendErrors),
    });
  }

  submitForm() {
    console.warn('Form value', this.form.value);
    const request: LoginRequest = {
      email: this.form.get('email')?.value,
      password: this.form.get('password')?.value,
    };
    this.store.dispatch(authActions.login({ request }));
  }
}
