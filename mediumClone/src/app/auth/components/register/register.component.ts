import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule,
} from '@angular/forms';
import { Router, RouterLink, RouterModule } from '@angular/router';
import { Store } from '@ngrx/store';
import { RegisterRequestI, UserI } from '../../types/registerRequest.interface';
import { Observable, combineLatest } from 'rxjs';
import { AuthState } from '../../types/authState.interfaces';
import { CommonModule } from '@angular/common';
import { selectBackendErrors, selectIsSubmitting } from '../../store/reducers';
import { AuthService } from '../../services/auth.service';
import { authActions } from '../../store/actions';
import { BackendErrorsI } from 'src/app/shared/types/backendErrors.interface';
import { BackendErrorMessagesComponent } from 'src/app/shared/components/backend-error-messages/backend-error-messages.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule,
    CommonModule,
    BackendErrorMessagesComponent,
  ],
})
export class RegisterComponent implements OnInit {
  form!: FormGroup;
  /*   isSubmitting$!: Observable<boolean>;
  backendErrors$!: Observable<BackendErrorsI | null>; */
  data$!: Observable<any>;
  constructor(
    private fb: FormBuilder,
    private store: Store<{ auth: AuthState }>,
    private authService: AuthService
  ) {}
  ngOnInit(): void {
    this.form = this.fb.nonNullable.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

    /*     this.isSubmitting$ = this.store.select(selectIsSubmitting);
    this.backendErrors$ = this.store.select(selectBackendErrors); */
    this.data$ = combineLatest({
      isSubmitting: this.store.select(selectIsSubmitting),
      backendErrors: this.store.select(selectBackendErrors),
    });
  }

  submitForm() {
    console.warn('Form value', this.form.value);
    const request: RegisterRequestI = { user: this.form.value };
    this.store.dispatch(authActions.register({ request }));
  }
}
