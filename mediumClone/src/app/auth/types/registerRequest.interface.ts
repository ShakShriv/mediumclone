import { CurrentUserI } from 'src/app/shared/types/currentUserInterface';
import { LoginRequest } from './loginRequest.type';

export interface RegisterRequestI {
  user: UserI;
}
export interface LoginRequestI {
  user: LoginRequest;
}
export interface UserI {
  username: string;
  email: string;
  password: string;
}

export interface CurrentUserReqI {
  user: Partial<CurrentUserI>;
}
