import { UserI } from './registerRequest.interface';

export type LoginRequest = Pick<UserI, 'password' | 'email'>;
