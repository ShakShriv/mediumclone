import { BackendErrorsI } from 'src/app/shared/types/backendErrors.interface';
import { CurrentUserI } from 'src/app/shared/types/currentUserInterface';

export interface AuthState {
  /**
   * permet de rendre le btn sign in disable, par defaut false
   */
  isSubmitting: boolean;
  currentUser: CurrentUserI | null | undefined;
  isLoading: boolean;
  backendErrors: BackendErrorsI | null;
}
