import { CurrentUserI } from 'src/app/shared/types/currentUserInterface';

export interface AuthResponseI {
  user: CurrentUserI;
}
