import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {
  LoginRequestI,
  RegisterRequestI,
} from '../types/registerRequest.interface';
import { Observable, map, catchError, throwError } from 'rxjs';
import { AuthResponseI } from '../types/authResponse.interface';
import { CurrentUserI } from 'src/app/shared/types/currentUserInterface';
import { environment } from 'src/environments/environment.development';
import { LoginRequest } from '../types/loginRequest.type';
import { CurrentUserReqI } from '../types/registerRequest.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}
  getUser(response: AuthResponseI): CurrentUserI {
    return response.user;
  }
  getCurrentUser(): Observable<CurrentUserI> {
    const url = environment.API_URL + environment.API_GET_CURRENT_USER;
    return this.http.get<AuthResponseI>(url).pipe(
      map(this.getUser),
      catchError((err: HttpErrorResponse) => {
        return throwError(() => err);
      })
    );
  }

  updateCurrentUser(user: Partial<CurrentUserI>): Observable<CurrentUserI> {
    const req: CurrentUserReqI = {
      user,
    };
    const url = environment.API_URL + environment.API_GET_CURRENT_USER;
    return this.http.put<AuthResponseI>(url, req).pipe(
      map(this.getUser),
      catchError((err: HttpErrorResponse) => {
        return throwError(() => err);
      })
    );
  }

  login(data: LoginRequest): Observable<CurrentUserI> {
    const url = environment.API_URL + environment.API_LOGIN;
    const loginReq: LoginRequestI = { user: data };
    return this.http.post<AuthResponseI>(url, loginReq).pipe(
      map(this.getUser),
      catchError((err: HttpErrorResponse) => {
        return throwError(() => err);
      })
    );
  }

  register(data: RegisterRequestI): Observable<CurrentUserI> {
    const url = environment.API_URL + '/users';
    return this.http.post<AuthResponseI>(url, data).pipe(
      map(this.getUser),
      catchError((err: HttpErrorResponse) => {
        console.error(err);
        return throwError(() => err);
      })
    ); // on veut juste le user
  }
}

/**  username: 'shak120996',
      email: 'shak@gmail.com',
      password: 'test'
    } */
