import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { ArticleI } from '../../shared/types/article.interface';
import { ArticleFormValueInterface } from 'src/app/shared/types/articleFormValue';

export const articlesActions = createActionGroup({
  source: 'Article',
  events: {
    'Get Article': props<{ slug: string }>(),
    'Get Article Sucess': props<{ article: ArticleI }>(),
    'Get Article Failure': emptyProps(),

    'Delete Article': props<{ slug: string }>(),
    'Delete Article Sucess': emptyProps(),
    'Delete Article Failure': emptyProps(),

    'Create Article': props<{ article: ArticleFormValueInterface }>(),
    'Create Article Sucess': props<{ article: ArticleI }>(),
    'Create Article Failure': emptyProps(),
  },
});
