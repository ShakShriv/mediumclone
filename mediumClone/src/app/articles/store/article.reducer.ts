import { createFeature, createReducer, on } from '@ngrx/store';
import { ArticleI } from 'src/app/shared/types/article.interface';
import { articlesActions } from './article.actions';
import { state } from '@angular/animations';
import { routerNavigationAction } from '@ngrx/router-store';

export interface ArticleState {
  article: ArticleI | null;
  isLoading: boolean;
  errors: any;
}

const initalState: ArticleState = {
  article: null,
  errors: null,
  isLoading: false,
};

const articleFeature = createFeature({
  name: 'article',
  reducer: createReducer(
    initalState,
    on(articlesActions.getArticle, (state, { slug }) => {
      return {
        ...state,
        isLoading: true,
      };
    }),
    on(articlesActions.getArticleSucess, (state, { article }) => {
      return {
        ...state,
        article,
        isLoading: false,
      };
    }),
    on(articlesActions.getArticleFailure, (state) => {
      return {
        ...state,
        isLoading: false,
        errors: 'error',
      };
    }),
    on(routerNavigationAction, (state) => initalState)
  ),
});
export const {
  name: articleFeatureKey,
  reducer: articleReducer,
  selectArticle,
  selectErrors,
  selectIsLoading,
} = articleFeature;
