import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ArticleService } from 'src/app/shared/services/Article.service';
import { articlesActions } from './article.actions';
import { catchError, concatMap, map, of, tap } from 'rxjs';
import { ArticleI } from 'src/app/shared/types/article.interface';
import { AuthEffect } from '../../auth/store/effects';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ArticleEffects {
  constructor(
    private articleService: ArticleService,
    private actions: Actions,
    private router: Router
  ) {}

  articleEffect$ = createEffect(() =>
    this.actions.pipe(
      ofType(articlesActions.getArticle),
      concatMap(({ slug }) =>
        this.articleService
          .getArticle(slug)
          .pipe(
            map((article: ArticleI) =>
              articlesActions.getArticleSucess({ article })
            )
          )
      ),
      catchError(() => of(articlesActions.getArticleFailure()))
    )
  );
  deleteArticleEffect$ = createEffect(() =>
    this.actions.pipe(
      ofType(articlesActions.deleteArticle),
      concatMap(({ slug }) =>
        this.articleService
          .deleteArticle(slug)
          .pipe(map((response) => articlesActions.deleteArticleSucess()))
      ),
      catchError(() => of(articlesActions.getArticleFailure()))
    )
  );

  redirectionAfterDeleteSucessEffect$ = createEffect(
    () =>
      this.actions.pipe(
        ofType(articlesActions.deleteArticle),
        tap((res) => this.router.navigate(['/']))
      ),
    { dispatch: false }
  );

  createArticleEffect$ = createEffect(() =>
    this.actions.pipe(
      ofType(articlesActions.createArticle),
      concatMap(({ article }) =>
        this.articleService
          .createArticle(article)
          .pipe(
            map((article) => articlesActions.createArticleSucess({ article }))
          )
      ),
      catchError(() => of(articlesActions.createArticleFailure()))
    )
  );

  redirectAfterCreateArticleSucess$ = createEffect(
    () =>
      this.actions.pipe(
        ofType(articlesActions.createArticleSucess),
        tap(({ article: { slug: newArticleSlug } }) =>
          this.router.navigate(['article', newArticleSlug])
        )
      ),
    { dispatch: false }
  );
}
