import { Routes } from '@angular/router';
import { ArticleComponent } from './components/article/article.component';
import { provideEffects } from '@ngrx/effects';
import { ArticleEffects } from './store/article.effects';
import { provideState } from '@ngrx/store';
import { articleFeatureKey, articleReducer } from './store/article.reducer';

export const routes: Routes = [
  {
    path: '',
    component: ArticleComponent,
    providers: [],
  },
];
