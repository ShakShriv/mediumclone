import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';
import { articlesActions } from '../../store/article.actions';
import { Observable, combineLatest, filter, map, tap } from 'rxjs';
import {
  selectArticle,
  selectErrors,
  selectIsLoading,
} from '../../store/article.reducer';
import { selectCurrentUser } from 'src/app/auth/store/reducers';
import { TagListComponent } from 'src/app/shared/components/tag-list/tag-list.component';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  standalone: true,
  imports: [RouterLink, ReactiveFormsModule, CommonModule, TagListComponent],
})
export class ArticleComponent implements OnInit {
  slug: string = '';
  data$!: Observable<any>;
  isAuthor$!: Observable<boolean>;
  constructor(private store: Store, private route: ActivatedRoute) {}
  deleteArticle(slug: string) {
    this.store.dispatch(articlesActions.deleteArticle({ slug }));
  }
  ngOnInit(): void {
    this.slug = this.route.snapshot.paramMap.get('slug') ?? '';
    this.store.dispatch(articlesActions.getArticle({ slug: this.slug }));

    this.isAuthor$ = combineLatest({
      article: this.store.select(selectArticle),
      currentUser: this.store
        .select(selectCurrentUser)
        .pipe(filter((user) => !!user)),
    }).pipe(
      tap((res) =>
        console.warn('[Article] ngOnInit isAuthor$ combineLastest Value', res)
      ),
      map(
        ({ article, currentUser }) =>
          article?.author.username === currentUser?.username
      )
    );

    this.data$ = combineLatest({
      article: this.store.select(selectArticle),
      error: this.store.select(selectErrors),
      isloading: this.store.select(selectIsLoading),
      isAuthor: this.isAuthor$,
    });
  }
}
