import { Routes } from '@angular/router';
import { GlobalFeedComponent } from './components/global-feed/global-feed.component';

export const globalRoutes: Routes = [
  {
    path: '',
    component: GlobalFeedComponent,
  },
];
