import { CommonModule, CurrencyPipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { RouterLink } from '@angular/router';
import { BackendErrorMessagesComponent } from '../../shared/components/backend-error-messages/backend-error-messages.component';
import { Store } from '@ngrx/store';
import {
  selectCurrentUser,
  selectIsSubmitting,
} from 'src/app/auth/store/reducers';
import {
  Subject,
  filter,
  map,
  takeUntil,
  tap,
  Observable,
  Subscription,
} from 'rxjs';
import {
  CurrentUserI,
  UpdtUser,
} from '../../shared/types/currentUserInterface';
import { authActions } from 'src/app/auth/store/actions';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule,
    CommonModule,
    BackendErrorMessagesComponent,
  ],
})
export class SettingsComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject();
  form!: FormGroup<{
    image: FormControl<string>;
    username: FormControl<string>;
    bio: FormControl<string>;
    email: FormControl<string>;
  }>;
  user!: CurrentUserI;
  user$!: Observable<CurrentUserI>;
  constructor(private fb: FormBuilder, private store: Store) {}

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
  ngOnInit(): void {
    this.initForm();
    this.user$ = this.store.select(selectCurrentUser).pipe(
      filter(Boolean),
      tap((user) => (this.user = user)),
      tap((user: CurrentUserI) => this.pachCurrentUserData(user)),
      tap(console.warn),
      takeUntil(this.destroy$)
    );
  }
  initForm() {
    this.form = this.fb.nonNullable.group({
      bio: '',
      image: '',
      email: '',
      username: '',
    });
  }

  pachCurrentUserData(user: CurrentUserI) {
    this.form.patchValue({
      image: user.image,
      email: user.email,
      bio: user.bio ?? '',
      username: user.username,
    });
  }
  submit() {
    const user: Partial<CurrentUserI> = {
      ...this.form.getRawValue(),
    };

    this.store.dispatch(authActions.updateCurrentUser({ user }));
  }
}
