import { bootstrapApplication } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { provideRouter } from '@angular/router';
import { appRoutes } from './app/app.routes';
import { provideState, provideStore } from '@ngrx/store';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { isDevMode } from '@angular/core';
import { authFeatureKey, authReducer } from './app/auth/store/reducers';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { AuthEffect } from './app/auth/store/effects';
import { provideEffects } from '@ngrx/effects';
import { provideRouterStore, routerReducer } from '@ngrx/router-store';
import { authInterceptor } from './app/shared/services/auth.interceptor';
import { FeedEffect } from './app/shared/components/feed/store/feedEffects';
import {
  feedFeatureKey,
  feedReducer,
} from './app/shared/components/feed/store/feedReducer';
import {
  popularTagFeatureKey,
  popularTagsReducer,
} from './app/shared/components/popular-tags/store/popularTags.reducer';
import { PopularTagsEffect } from './app/shared/components/popular-tags/store/popular-tags-effect.service';
import {
  articleFeatureKey,
  articleReducer,
} from './app/articles/store/article.reducer';
import { ArticleEffects } from './app/articles/store/article.effects';


bootstrapApplication(AppComponent, {
  providers: [
    provideHttpClient(withInterceptors([authInterceptor])),
    provideRouter(appRoutes),
    provideStore(
      {
        router: routerReducer,
      },
      {
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
          strictActionTypeUniqueness: true,
        },
      }
    ),
    provideRouterStore(),
    provideEffects(AuthEffect, FeedEffect, PopularTagsEffect, ArticleEffects),
    provideState(articleFeatureKey, articleReducer),
    provideState(authFeatureKey, authReducer),
    provideState(feedFeatureKey, feedReducer),
    provideState(popularTagFeatureKey, popularTagsReducer),
    provideStoreDevtools({ maxAge: 25, logOnly: !isDevMode() }),
  ],
});
