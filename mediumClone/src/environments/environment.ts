export const environment = {
  API_URL: 'https://api.realworld.io/api',
  API_LOGIN: '/users/login',
  API_GET_CURRENT_USER: '/user',

  limitPagination: 20,
};
